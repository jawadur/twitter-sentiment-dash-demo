import os
import sys
import logging
import plotly
import sqlite3
import string
import time
import pickle
import flask
import glob
import base64
import folium
import dash
import dash_core_components as dcc
import dash_html_components as html
import numpy as np
import plotly.graph_objs as go
import pandas as pd
import regex as re
from cache import cache
from config import stop_words
from dash.dependencies import Output, Event, Input, State
from wordcloud import WordCloud, STOPWORDS
from collections import Counter
from io import BytesIO
from folium import plugins
from PIL import Image

# set chdir to current dir
sys.path.insert(0, os.path.realpath(os.path.dirname(__file__)))
os.chdir(os.path.realpath(os.path.dirname(__file__)))

logging.basicConfig(format='%(asctime)s %(levelname)s %(message)s', filename='app.log', level=logging.ERROR)

# it's ok to use one shared sqlite connection
# as we are making selects only, no need for any kind of serialization as well
conn = sqlite3.connect('twitter.db', check_same_thread=False)

punctuation = [str(i) for i in string.punctuation]

sentiment_colors = {-1:"#EE6055",
                    -0.5:"#FDE74C",
                     0:"#FFE6AC",
                     0.5:"#D0F2DF",
                     1:"#9CEC5B",}

app_colors = {
    'background': '#0C0F0A',
    'text': '#FFFFFF',
    'sentiment-plot':'#41EAD4',
    'volume-bar':'#FBFC74',
    'someothercolor':'#FF206E',
}

POS_NEG_NEUT = 0.1

MAX_DF_LENGTH = 100

thumbs_up_mask = np.array(Image.open('./resources/thumbs-up.jpg'))
thumbs_down_mask = np.array(Image.open('./resources/thumbs-down.jpg'))

sentiment_term = 'twitter'

app = dash.Dash(__name__)

app.layout = html.Div(
       [html.H2('Live Twitter Sentiment', style={'color':"#CECECE", 'margin-left':10, 'margin-right':10}),
                                            html.Div(className='row', children=[
                                                dcc.Input(id='sentiment_term', value='twitter', type='text',
                                                        style={'color':app_colors['someothercolor'],
                                                                'font-size': '24px', 'width':'20%', 'border-radius':'5px',
                                                                'border-style':'solid', 'border-size':'0.1em',
                                                                'border-color':'#CECECE', 'padding-left':10}),
                                            html.Button('Search', id='search-button', style={'width':105, 'font-size': '24px',
                                                                                             'margin-left':10, 'padding':1,
                                                                                             'color': '#CECECE',
                                                                                             'background-color': 'transparent'}),],
                                                    style={'margin-left':10, 'margin-top':50}),

        html.Div(className='row', children=[html.Div(id='related-sentiment',
                                                        children=html.Button('Loading related terms...',
                                                                                id='related_term_button'),
                                                    className='col s12 m6 l6', style={"word-wrap":"break-word"}),
                                            html.Div(id='recent-trending',
                                                    className='col s12 m6 l6', style={"word-wrap":"break-word"})]),

        html.Div(className='row', children=[html.Div(dcc.Graph(id='live-graph', config={'displayModeBar': False},
                                                                animate=False),
                                                    className='col s12 m6 l6'),
                                            html.Div(dcc.Graph(id='historical-graph', config={'displayModeBar': False},
                                                                animate=False),
                                                    className='col s12 m6 l6')]),

        html.Div(className='row', children=[html.Div(id="recent-tweets-table", className='col s12 m6 l6'),
                                            html.Div(dcc.Graph(id='sentiment-pie', config={'displayModeBar': False},
                                                                animate=False, style={'height': '800px'}),
                                                    className='col s12 m6 l6'),]),

        html.Div(className='row', children=[html.Div(id='thumbs-up-div', className='col s12 m6 l6'),
                                            html.Div(id='thumbs-down-div', className='col s12 m6 l6'),],
                style={'margin-top': 50}),

        html.Div(className='row', children=[html.Div(id='map-div')],
                style={'margin-left':'1%', 'margin-right':'1%', 'margin-top': 50}),

        html.Div(id='sentiment_term_cache', style={'display': 'none'}),

        dcc.Interval(
            id='graph-update',
            interval=5*1000
        ),
        dcc.Interval(
            id='historical-update',
            interval=60*1000
        ),
        dcc.Interval(
            id='related-update',
            interval=60*1000
        ),
        dcc.Interval(
            id='recent-update',
            interval=60*1000
        ),
        dcc.Interval(
            id='recent-table-update',
            interval=5*1000
        ),
        dcc.Interval(
            id='sentiment-pie-update',
            interval=60*1000
        ),
        dcc.Interval(
            id='thumbs-up-update',
            interval=60*1000
        ),
        dcc.Interval(
            id='thumbs-down-update',
            interval=60*1000
        ),
        dcc.Interval(
            id='map-update',
            interval=60*1000
        ),

    ], style={'backgroundColor': app_colors['background'], 'margin-top':'-30px', 'height':'3500px',},
)

def df_resample_sizes(df, maxlen=MAX_DF_LENGTH):
    df_len = len(df)
    resample_amt = 100
    vol_df = df.copy()
    vol_df['volume'] = 1

    if df_len <= 1:
        return df

    ms_span = (df.index[-1] - df.index[0]).seconds * 1000
    rs = int(ms_span / maxlen)
    df = df.resample('{}ms'.format(int(rs))).mean()
    df.dropna(inplace=True)
    vol_df = vol_df.resample('{}ms'.format(int(rs))).sum()
    vol_df.dropna(inplace=True)
    df = df.join(vol_df['volume'])

    return df

# make a counter with blacklist words and empty word with some big value - we'll use it later to filter counter
stop_words.append('')
blacklist_counter = Counter(dict(zip(stop_words, [1000000]*len(stop_words))))

# complie a regex for split operations (punctuation list, plus space and new line)
split_regex = re.compile("[ \n"+re.escape("".join(punctuation))+']')

def related_sentiments(df, sentiment_term, how_many=15):

    try:
        related_words = {}
        # it's way faster to join strings to one string
        # then use regex split using the punctuation list plus space and new line chars
        # regex precomiled above
        tokens = split_regex.split(' '.join(df['tweet'].values.tolist()).lower())
        # it is way faster to remove stop_words, sentiment_term and empty token by making another counter
        # with some big value and substracting (counter will substract and remove tokens with negative count)
        blacklist_counter_with_term = blacklist_counter.copy()
        blacklist_counter_with_term[sentiment_term] = 1000000
        counts = (Counter(tokens) - blacklist_counter_with_term).most_common(15)

        for term,count in counts:
            try:
                df = pd.read_sql("""
                                SELECT sentiment.* FROM  sentiment_fts
                                fts LEFT JOIN sentiment
                                ON fts.rowid = sentiment.id
                                WHERE fts.sentiment_fts MATCH ?
                                ORDER BY fts.rowid DESC
                                LIMIT 200
                                """, conn, params=(term,))

                related_words[term] = [df['sentiment'].mean(), count]
            except Exception as e:
                logging.error('\n\n', exc_info=True)

        return related_words

    except Exception as e:
        logging.error('\n\n', exc_info=True)

def quick_color(s):
    # except return bg as app_colors['background']
    if s >= POS_NEG_NEUT:
        # positive
        return "#002C0D"
    elif s <= -POS_NEG_NEUT:
        # negative:
        return "#270000"
    else:
        return app_colors['background']

def generate_table(df, max_rows=10):
    return html.Table(className="responsive-table",
                      children=[
                          html.Thead(
                              html.Tr(
                                  children=[
                                      html.Th(col.title()) for col in df.columns.values],
                                  style={'color':app_colors['text']}
                                  )
                              ),
                          html.Tbody(
                              [

                              html.Tr(
                                  children=[
                                      html.Td(data) for data in d
                                      ], style={'color':app_colors['text'],
                                                'background-color':quick_color(d[2])}
                                  )
                               for d in df.values.tolist()])
                          ]
    )

def pos_neg_neutral(col):
    if col >= POS_NEG_NEUT:
        # positive
        return 1
    elif col <= -POS_NEG_NEUT:
        # negative:
        return -1
    else:
        return 0

max_size_change = 0.4

def generate_size(value, smin, smax):
    size_change = round((( (value-smin) /smax)*2) - 1, 2)
    final_size = (size_change*max_size_change) + 1
    return final_size*180

def full_text_search(search_term, max_rows):
    if search_term:
        df = pd.read_sql("""
                        SELECT sentiment.* FROM  sentiment_fts
                        fts LEFT JOIN sentiment
                        ON fts.rowid = sentiment.id
                        WHERE fts.sentiment_fts MATCH ?
                        ORDER BY fts.rowid DESC
                        LIMIT {}
                        """.format(str(max_rows)), conn, params=(search_term+'*',))
    else:
        df = pd.read_sql("SELECT * FROM sentiment ORDER BY id DESC, unix DESC LIMIT {}".format(max_rows), conn)

    return df


@app.callback(Output('sentiment_term_cache', 'children'),
              [Input(component_id='search-button', component_property='n_clicks')],
              [State('sentiment_term', 'value')])
def update_sentiment_term_cache(n_clicks, sentiment_term):
    return sentiment_term


@app.callback(Output('recent-tweets-table', 'children'),
              [Input(component_id='sentiment_term_cache', component_property='children')],
              events=[Event('recent-table-update', 'interval')])
def update_recent_tweets(sentiment_term):
    df = full_text_search(sentiment_term, 10)
    df['date'] = pd.to_datetime(df['unix'], unit='ms')
    df = df.drop(['unix','id'], axis=1)
    df = df[['date','tweet','sentiment']]

    return generate_table(df, max_rows=10)


@app.callback(Output('sentiment-pie', 'figure'),
              [Input(component_id='sentiment_term_cache', component_property='children')],
              events=[Event('sentiment-pie-update', 'interval')])
def update_pie_chart(sentiment_term):
    sentiment_pie_dict = dict()
    # get data from cache
    for i in range(100):
        try:
            sentiment_pie_dict = cache.get('sentiment_shares', sentiment_term)
        except Exception as e:
            logging.error('\n\n', exc_info=True)

        if sentiment_pie_dict:
            break
        time.sleep(0.1)

    labels = ['Positive','Negative']

    try: pos = sentiment_pie_dict[1]
    except: pos = 0

    try: neg = sentiment_pie_dict[-1]
    except: neg = 0

    values = [pos,neg]
    colors = ['#007F25', '#800000']

    trace = go.Pie(labels=labels, values=values,
                   hoverinfo='label+percent', textinfo='value',
                   hole = 0.6,
                   textfont=dict(size=20, color=app_colors['text']),
                   marker=dict(colors=colors,
                               line=dict(color=app_colors['background'], width=2)))

    return {"data":[trace],'layout' : go.Layout(
                                                  title='Positive vs Negative sentiment',
                                                  font={'color':app_colors['text']},
                                                  annotations=[{"font": {
                                                                        "size": 20, "color": app_colors['text']},
                                                                'text':'"' + sentiment_term + '"', 'x':0.5, 'y':0.5,
                                                                'showarrow': False}],
                                                  plot_bgcolor = app_colors['background'],
                                                  paper_bgcolor = app_colors['background'],
                                                  showlegend=True)}


@app.callback(Output('live-graph', 'figure'),
              [Input(component_id='sentiment_term_cache', component_property='children')],
              events=[Event('graph-update', 'interval')])
def update_graph_scatter(sentiment_term):

    try:
        df = full_text_search(sentiment_term, 1000)
        df.sort_values('unix', inplace=True)
        df['date'] = pd.to_datetime(df['unix'], unit='ms')
        df.set_index('date', inplace=True)
        df['sentiment_smoothed'] = df['sentiment'].rolling(int(len(df)/5)).mean()
        df = df_resample_sizes(df)
        init_length = len(df)

        if init_length <= 1:
            return None

        X = df.index
        Y = df.sentiment_smoothed.values
        Y2 = df.volume.values
        data = plotly.graph_objs.Scatter(
                x=X,
                y=Y,
                name='Sentiment',
                mode= 'lines',
                yaxis='y2',
                line = dict(color = (app_colors['sentiment-plot']),
                            width = 4,)
                )

        data2 = plotly.graph_objs.Bar(
                x=X,
                y=Y2,
                name='Volume',
                marker=dict(color=app_colors['volume-bar']),
                )

        return {'data': [data,data2],'layout' : go.Layout(xaxis=dict(range=[min(X),max(X)]),
                                                          yaxis=dict(range=[min(Y2),max(Y2*4)],
                                                                        title='Volume', side='right'),
                                                          yaxis2=dict(range=[min(Y),max(Y)],
                                                                        side='left', overlaying='y', title='Sentiment'),
                                                          title='Live sentiment for: "{}"'.format(sentiment_term),
                                                          font={'color':app_colors['text']},
                                                          plot_bgcolor = app_colors['background'],
                                                          paper_bgcolor = app_colors['background'],
                                                          showlegend=False)}

    except Exception as e:
        logging.error('\n\n', exc_info=True)


@app.callback(Output('historical-graph', 'figure'),
              [Input(component_id='sentiment_term_cache', component_property='children')],
              events=[Event('historical-update', 'interval')])
def update_hist_graph_scatter(sentiment_term):

    try:
        df = full_text_search(sentiment_term, 10000)
        df.sort_values('unix', inplace=True)
        df['date'] = pd.to_datetime(df['unix'], unit='ms')
        df.set_index('date', inplace=True)
        # save this to a file, then have another function that
        # updates because of this, using intervals to read the file.
        # https://community.plot.ly/t/multiple-outputs-from-single-input-with-one-callback/4970
        # store related sentiments in cache
        cache.set('related_terms', sentiment_term, related_sentiments(df, sentiment_term), 120)
        init_length = len(df)
        df['sentiment_smoothed'] = df['sentiment'].rolling(int(len(df)/5)).mean()
        df.dropna(inplace=True)
        df = df_resample_sizes(df,maxlen=500)
        init_length = len(df)

        if init_length <= 1:
            return None

        X = df.index
        Y = df.sentiment_smoothed.values
        Y2 = df.volume.values

        data = plotly.graph_objs.Scatter(
                x=X,
                y=Y,
                name='Sentiment',
                mode= 'lines',
                yaxis='y2',
                line = dict(color = (app_colors['sentiment-plot']),
                            width = 4,)
                )

        data2 = plotly.graph_objs.Bar(
                x=X,
                y=Y2,
                name='Volume',
                marker=dict(color=app_colors['volume-bar']),
                )

        df['sentiment_shares'] = list(map(pos_neg_neutral, df['sentiment']))
        cache.set('sentiment_shares', sentiment_term, dict(df['sentiment_shares'].value_counts()), 120)

        return {'data': [data,data2],'layout' : go.Layout(xaxis=dict(range=[min(X),max(X)]), # add type='category to remove gaps'
                                                          yaxis=dict(range=[min(Y2),max(Y2*4)], title='Volume', side='right'),
                                                          yaxis2=dict(range=[min(Y),max(Y)], side='left',
                                                                        overlaying='y', title='Sentiment'),
                                                          title='Longer-term sentiment for: "{}"'.format(sentiment_term),
                                                          font={'color':app_colors['text']},
                                                          plot_bgcolor = app_colors['background'],
                                                          paper_bgcolor = app_colors['background'],
                                                          showlegend=False)}

    except Exception as e:
        logging.error('\n\n', exc_info=True)


# SINCE A SINGLE FUNCTION CANNOT UPDATE MULTIPLE OUTPUTS...
#https://community.plot.ly/t/multiple-outputs-from-single-input-with-one-callback/4970
@app.callback(Output('related-sentiment', 'children'),
              [Input(component_id='sentiment_term_cache', component_property='children')],
              events=[Event('related-update', 'interval')])
def update_related_terms(sentiment_term):

    try:
        # get data from cache
        for i in range(100):
            related_terms = cache.get('related_terms', sentiment_term) # term: {mean sentiment, count}
            if related_terms:
                break
            time.sleep(0.1)

        if related_terms is None or len(related_terms) <= 1:
            return None

        buttons = [html.Button('{}({})'.format(term, related_terms[term][1]),
                                id='related_term_button', value=term, className='btn', type='submit',
                                style={'background-color':'#4CBFE1', 'margin-right':'5px', 'margin-top':'5px'})
                    for term in related_terms]

        sizes = [related_terms[term][1] for term in related_terms]
        smin = min(sizes)
        smax = max(sizes) - smin

        buttons = [html.H5('Terms related to "{}": '.format(sentiment_term), style={'color':app_colors['text']})]\
                    +[html.Span(term, style={'color':sentiment_colors[round(related_terms[term][0]*2)/2],
                                                'margin-right':'15px', 'margin-top':'15px', 'font-size':'{}%'\
                                                .format(generate_size(related_terms[term][1], smin, smax))})
                    for term in related_terms]

        return buttons

    except Exception as e:
        logging.error('\n\n', exc_info=True)


@app.callback(Output('recent-trending', 'children'),
              [Input(component_id='sentiment_term_cache', component_property='children')],
              events=[Event('recent-update', 'interval')])
def update_recent_trending(sentiment_term):
    try:
        query = """
                SELECT
                        value
                FROM
                        misc
                WHERE
                        key = 'trending'
        """

        c = conn.cursor()
        result = c.execute(query).fetchone()
        related_terms = pickle.loads(result[0])
        sizes = [related_terms[term][1] for term in related_terms]
        smin = min(sizes)
        smax = max(sizes) - smin

        buttons = [html.H5('Recently Trending Terms: ', style={'color':app_colors['text']})]\
                    +[html.Span(term, style={'color':sentiment_colors[round(related_terms[term][0]*2)/2],
                                                'margin-right':'15px', 'margin-top':'15px', 'font-size':'{}%'\
                                                .format(generate_size(related_terms[term][1], smin, smax))})
                    for term in related_terms]

        return buttons

    except Exception as e:
        logging.error('\n\n', exc_info=True)


@app.callback(Output(component_id='thumbs-up-div', component_property='children'),
              [Input(component_id='sentiment_term_cache', component_property='children')],
              events=[Event('thumbs-up-update', 'interval')])
def update_thumbs_up(sentiment_term):
    df = full_text_search(sentiment_term, 1000)
    df = df[df['sentiment'] >= 0.1]
    df = df.drop(['unix','id','sentiment'], axis=1)
    df = df[['tweet']]
    # join tweets to a single string
    words = ' '.join(df['tweet'])

    # remove URLs, RTs, and twitter handles
    no_urls_no_tags = " ".join([word for word in words.split()
                                if 'http' not in word
                                    and not word.startswith('@')
                                    and word != 'RT'
                                ])

    if len(no_urls_no_tags) < 2:
        no_urls_no_tags = sentiment_term
    wordcloud = WordCloud(
                          font_path='./resources/Ubuntu-R.ttf',
                          stopwords=STOPWORDS,
                          background_color=app_colors['background'],
                          mask=thumbs_up_mask,
                         ).generate(no_urls_no_tags)

    wc_array = wordcloud.to_array()
    image = Image.fromarray(wc_array)
    byte_io = BytesIO()
    image.save(byte_io, 'PNG')
    byte_io.seek(0)
    encoded_image = base64.b64encode(byte_io.read())
    byte_io.close()

    return [html.Div('Positive sentiment Word Cloud for: "{}"'.format(sentiment_term),
                    style={'color': app_colors['text'], 'text-align': 'center',
                            'margin-left':'1%', 'margin-right':'1%', 'fontSize':'18px'}),
            html.Img(src='data:image/png;base64,{}'.format(encoded_image.decode()),
                                style={'width': '60%', 'margin-left':'20%',
                                        'margin-right':'20%', 'margin-top':50})]


@app.callback(Output(component_id='thumbs-down-div', component_property='children'),
              [Input(component_id='sentiment_term_cache', component_property='children')],
              events=[Event('thumbs-down-update', 'interval')])
def update_thumbs_down(sentiment_term):
    df = full_text_search(sentiment_term, 1000)
    df = df[df['sentiment'] <= 0.1]
    df = df.drop(['unix','id','sentiment'], axis=1)
    df = df[['tweet']]
    # join tweets to a single string
    words = ' '.join(df['tweet'])

    # remove URLs, RTs, and twitter handles
    no_urls_no_tags = " ".join([word for word in words.split()
                                if 'http' not in word
                                    and not word.startswith('@')
                                    and word != 'RT'
                                ])

    if len(no_urls_no_tags) < 2:
        no_urls_no_tags = sentiment_term
    wordcloud = WordCloud(
                          font_path='./resources/Ubuntu-R.ttf',
                          stopwords=STOPWORDS,
                          background_color=app_colors['background'],
                          mask=thumbs_down_mask,
                         ).generate(no_urls_no_tags)

    wc_array = wordcloud.to_array()
    image = Image.fromarray(wc_array)
    byte_io = BytesIO()
    image.save(byte_io, 'PNG')
    byte_io.seek(0)
    encoded_image = base64.b64encode(byte_io.read())
    byte_io.close()

    return [html.Div('Negative sentiment Word Cloud for: "{}"'.format(sentiment_term),
                    style={'color': app_colors['text'], 'text-align': 'center',
                            'margin-left':'1%', 'margin-right':'1%', 'fontSize':'18px'}),
            html.Img(src='data:image/png;base64,{}'.format(encoded_image.decode()),
                                style={'width': '60%', 'margin-left':'20%',
                                        'margin-right':'20%', 'margin-top':50})]


@app.callback(Output('map-div', 'children'),
              [Input(component_id='sentiment_term_cache', component_property='children')],
              events=[Event('map-update', 'interval')])
def update_map(sentiment_term):
    df = full_text_search(sentiment_term, 10000)
    df = df[df['lat'].notnull()]
    heatmap_map = folium.Map(tiles='https://cartodb-basemaps-{s}.global.ssl.fastly.net/dark_all/{z}/{x}/{y}.png',
                                location=[17.433074, 8.959470], zoom_start=3,
                                attr='(c) <a href="http://cartodb.com/attributions">CartoDB</a>')
    heatmap_map.add_child(plugins.HeatMap(list(zip(df.lat, df.lon))))
    html_string = heatmap_map.get_root().render()

    return [html.H5('Global twitter activity for: "{}"'.format(sentiment_term),
                        style={'color': app_colors['text']}),
            html.Iframe(className='col s12', srcDoc = flask.render_template_string(html_string),
                        style={'height': '800px'})]


external_css = ["https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css"]
for css in external_css:
    app.css.append_css({"external_url": css})


external_js = ['https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js',
               'https://pythonprogramming.net/static/socialsentiment/googleanalytics.js']
for js in external_js:
    app.scripts.append_script({'external_url': js})


server = app.server
dev_server = app.run_server
