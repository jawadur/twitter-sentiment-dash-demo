# Social Sentiment Dash Application Demo
Cloned from [**this repo**](https://github.com/Sentdex/socialsentiment/).
Original README can be found in above link. This one has been slightly modified.

Original app hosted at [**SocialSentiment.net**](http://socialsentiment.net/).

Original app screenshot:
![application example](https://pythonprogramming.net/static/images/dash/dashapplication.jpg)

## Changes and additions made:
- Changed module name `dash_mess.py` to `app.py`
- sqlite-based cache part (used with Flask development server) in `cache.py` was buggy. Fixes added.
- Two new charts have been added: "Word Cloud" and "Density Map (heatmap)". Note: These chart types are not available in plotly. So, had to resort to some workarounds.
- For Density Map, modified `twitter_stream.py` to collect and store geographic coordinates data as well.
- Default behavior/property of Dash's core component "Input" is "OnChange". That cannot be modified (might be possible to change to OnBlur but have not tried that since that would not be much useful either). This posed a major issue. Every time the user types a single letter, the reactive Input triggers a database query+search for the prefix/subterm of the intended full search-term, e.g. typing "dog", a db query+search would be perfomed for 'd', then for 'do' and finally 'dog'. So, added a submit button, and linked the button Input and search-term State to all the callbacks. Used a hidden div to cache the search-term.
- Made minor changes to the overall design.
- Changed update Intervals of most of the Events.
- Refactored code.

Screenshot of app after the above changes:\
![updated application](http://i64.tinypic.com/30uuxs1.jpg)

App URL to be added after deployment.

## Repo Contents:
- `app.py` - This is currently the main front-end application code. Contains the dash application layouts, logic for graphs, interfaces with the database...etc. This code is setup to run on a flask instance. If you want to clone this and run it locally, you will be using the `dev_server.py`
- `dev_server.py` - If you wish to run this application locally, on the dev server, run via this instead.
- `twitter_stream.py` - This should run in the background of your application. This is what streams tweets from Twitter, storing them into the sqlite database, which is what the `app.py` file interfaces with.
- `config.py` - Meant for many configurations, but right now it just contains stop words. Words we don't intend to ever count in the "trending"
- `cache.py` -  For caching purposes in effort to get things to run faster.
- `db-truncate.py` - A script to truncate the infinitely-growing sqlite database. You will get about 3.5 millionish tweets per day, depending on how fast you can process. You can keep these, but, as the database grows, search times will dramatically suffer.

## Quick start
- Python3.5.2
- install `requirements.txt` using `pip install -r requirements.txt`
- Fill in your Twitter App credentials to `twitter_stream.py` (Note: Valid credentials already added..intentionally left the credentials inside code)
- Run `twitter_stream.py` to build database
- If you're using this locally, you can run the application with the `dev_server.py` script.
- You might need the latest version of sqlite.
```
sudo add-apt-repository ppa:jonathonf/backports
sudo apt-get update && sudo apt-get install sqlite3
```
- Consider running the `db-truncate.py` from time to time (or via a cronjob), to keep the database reasonably sized. In its current state, the database really doesn't need to store more than 2-3 days of data most likely.

### Tips for Running on Server
- You can use Gunicorn to run the server
```
gunicorn app:server -b 0.0.0.0:80 -w 4
```
- Note that this project uses sqlite database. Hence, it should not be deployed on Heroku. 2-3 days data would be required in database for best results.
