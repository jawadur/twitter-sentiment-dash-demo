import os
import sys
import json
import logging
import sqlite3
import time
import string
import pickle
import itertools
import pandas as pd
import regex as re
import numpy as np
from vaderSentiment.vaderSentiment import SentimentIntensityAnalyzer
from tweepy import Stream
from tweepy import OAuthHandler
from tweepy.streaming import StreamListener
from unidecode import unidecode
from threading import Lock, Timer
from config import stop_words
from collections import Counter
from textblob import TextBlob

logging.basicConfig(format='%(asctime)s %(levelname)s %(message)s', filename='twitter_stream.log', level=logging.ERROR)

# set chdir to current dir
sys.path.insert(0, os.path.realpath(os.path.dirname(__file__)))
os.chdir(os.path.realpath(os.path.dirname(__file__)))

analyzer = SentimentIntensityAnalyzer()

# consumer key, consumer secret, access token, access secret.
# https://apps.twitter.com/ to setup
ckey="C03UMtbwxtVmx7SN0Jz19dse6"
csecret="6yPHwuoXydetIHtU2l7UW5KdpUgpnSCi5cBJEq5iZZAe6RcOir"
atoken="541184129-yjW59E7xcV19ZibMv8Z4baUkBF5AB06XjiBuX3XL"
asecret="5aGAjTh7Mfg6rVDYTQPD3SxWgcpvUwPSjlPyFPXXfac6L"

# isolation lever disables automatic transactions,
# we are disabling thread check as we are creating connection here, but we'll be inserting from a separate thread (no need for serialization)
conn = sqlite3.connect('twitter.db', isolation_level=None, check_same_thread=False)
c = conn.cursor()

def create_table():

    try:
        # http://www.sqlite.org/pragma.html#pragma_journal_mode
        # for us - it allows concurrent write and reads
        c.execute("PRAGMA journal_mode=wal")
        c.execute("PRAGMA wal_checkpoint=TRUNCATE")
        # included geographic data (lat, lon) here as well since extending app to include density map plot
        c.execute("CREATE TABLE IF NOT EXISTS sentiment(id INTEGER PRIMARY KEY AUTOINCREMENT, unix INTEGER, tweet TEXT, sentiment REAL, lon REAL, lat REAL)")
        # key-value table for random stuff
        c.execute("CREATE TABLE IF NOT EXISTS misc(key TEXT PRIMARY KEY, value TEXT)")
        # id on index, both as DESC (sorting in DESC order)
        c.execute("CREATE INDEX IF NOT EXISTS id_unix ON sentiment (id DESC, unix DESC)")
        # out full-text search table, chose to create data from external (content) table - sentiment
        # instead of directly inserting to that table, as saving more data than just text
        # https://sqlite.org/fts5.html - 4.4.2
        c.execute("CREATE VIRTUAL TABLE sentiment_fts USING fts5(tweet, content=sentiment, content_rowid=id, prefix=1, prefix=2, prefix=3)")
        # that trigger will automagically update out table when row is interted
        # (requires additional triggers on update and delete)
        c.execute("""
            CREATE TRIGGER sentiment_insert AFTER INSERT ON sentiment BEGIN
                INSERT INTO sentiment_fts(rowid, tweet) VALUES (new.id, new.tweet);
            END
        """)

    except Exception as e:
        logging.error('\n\n', exc_info=True)

create_table()

# create lock
lock = Lock()

class listener(StreamListener):

    data = []
    lock = None

    def __init__(self, lock):

        # create lock
        self.lock = lock

        # init timer for database save
        self.save_in_database()

        # call __init__ of super class
        super().__init__()

    def save_in_database(self):

        # set a timer (1 second)
        Timer(1, self.save_in_database).start()

        # with lock, if there's data, save in transaction using one bulk query
        with self.lock:
            if len(self.data):
                c.execute('BEGIN TRANSACTION')
                try:
                    c.executemany("INSERT INTO sentiment (unix, tweet, sentiment, lon, lat) VALUES (?, ?, ?, ?, ?)", self.data)
                except:
                    pass
                c.execute('COMMIT')

                self.data = []

    def on_data(self, data):

        try:
            data = json.loads(data)
            # there are records like that:
            # {'limit': {'track': 14667, 'timestamp_ms': '1520216832822'}}
            if 'truncated' not in data:
                return True
            if data['truncated']:
                tweet = unidecode(data['extended_tweet']['full_text'])
            else:
                tweet = unidecode(data['text'])
            time_ms = data['timestamp_ms']
            if data['place'] is not None:
                lon = data['place']['bounding_box']['coordinates'][0][0][0]
                lat = data['place']['bounding_box']['coordinates'][0][0][1]
            else:
                lon = np.nan
                lat = np.nan
            vs = analyzer.polarity_scores(tweet)
            sentiment = vs['compound']
            # append to data list (to be saved every 1 second)
            with self.lock:
                self.data.append((time_ms, tweet, sentiment, lon, lat))

        except KeyError as e:
            logging.error('\n\n', exc_info=True)
        return True

    def on_error(self, status):
        print(status)


# make a counter with blacklist words and empty word with some big value - used later to filter counter
stop_words.append('')
blacklist_counter = Counter(dict(zip(stop_words, [1000000] * len(stop_words))))

# complie a regex for split operations (punctuation list, plus space and new line)
punctuation = [str(i) for i in string.punctuation]
split_regex = re.compile("[ \n" + re.escape("".join(punctuation)) + ']')

def map_nouns(col):
    return [word[0] for word in TextBlob(col).tags if word[1] == u'NNP']

def generate_trending():

    try:
        # select last 10k tweets
        df = pd.read_sql("SELECT * FROM sentiment ORDER BY id DESC, unix DESC LIMIT 10000", conn)
        df['nouns'] = list(map(map_nouns, df['tweet']))

        # make tokens
        tokens = split_regex.split(' '.join(list(itertools.chain.from_iterable(df['nouns'].values.tolist()))).lower())
        # clean and get top 10
        trending = (Counter(tokens) - blacklist_counter).most_common(10)

        # get sentiments
        trending_with_sentiment = {}
        for term, count in trending:
            df = pd.read_sql("SELECT sentiment.* FROM  sentiment_fts fts LEFT JOIN sentiment ON fts.rowid = sentiment.id WHERE fts.sentiment_fts MATCH ? ORDER BY fts.rowid DESC LIMIT 1000", conn, params=(term,))
            trending_with_sentiment[term] = [df['sentiment'].mean(), count]

        # save in a database
        with lock:
            c.execute('BEGIN TRANSACTION')
            try:
                c.execute("REPLACE INTO misc (key, value) VALUES ('trending', ?)", (pickle.dumps(trending_with_sentiment),))
            except:
                pass
            c.execute('COMMIT')

    except Exception as e:
        logging.error('\n\n', exc_info=True)
    finally:
        Timer(5, generate_trending).start()

Timer(1, generate_trending).start()

while True:

    try:
        auth = OAuthHandler(ckey, csecret)
        auth.set_access_token(atoken, asecret)
        twitterStream = Stream(auth, listener(lock))
        twitterStream.filter(track=["a","e","i","o","u"])
    except Exception as e:
        logging.error('\n\n', exc_info=True)
        time.sleep(5)
